    // jsSocials
    $(".share").jsSocials({
        showLabel: false,
        showCount: false,
        shares: ["email", "twitter", "facebook", "whatsapp"]
    });
    
    // RateYo
    $("#rateYo").rateYo({
        rating: 3
    });
    
    // Gallery Initial Load
    var gallery = $('#thumbnails').find('.thumbnail-items:first');
    
    if(gallery.is('img')) {   
        var img = $('<img class="img-responsive">'); 
        var imgSrc= $('.thumbnail-items').attr('src');
        img.attr('src', imgSrc);
        img.appendTo('#gallery .main');    
    } else if (gallery.is('video')) {   
        var videoSrc = $('.thumbnail-items').attr('src');
        var video = `<video src="${videoSrc}" width="100%" class="" controls autoplay></video>`;
        $('#gallery .main').html(video)
    }
    
    // Thumbnail Clicked
    $('.thumbnail-items').on("click", function () {
        if($(this).is('img')){
            var currentImage = $(this).attr('src');
            var image = `<img src=${currentImage} class="img-responsive" alt="">`
            $('#gallery .main').html(image)
        } else if ($(this).is('video')) {
            var videoSrc = $(this).attr('src');
            var video = `<video src="${videoSrc}" width="100%" poster="https://i.ytimg.com/vi/x4wAT5BFJBQ/maxresdefault.jpg" class="" autoplay controls></video>`;
            $('#gallery .main').html(video)
        }
    }) 
    
    $(".slick-carousel").slick({
        dots: false,
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        // variableWidth: true,    
        autoplay: false,        
        pauseOnHover: false,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 5000,
                settings: {
                    vertical:true,
                    verticalSwiping:true,                  
                }
            },            
            {
                breakpoint: 990,
                settings: {
                    vertical:false,
                    verticalSwiping:false,   
                }
            },
        ]
    }); 
    
    $(window).on('resize orientationchange', function() {
        $('.slick-carousel').slick('reinit');
    });
    
    // Slick Arrows
    $('.arrow-right').click(function(){
        $('.slick-carousel').slick("slickNext");
    });
    
    $('.arrow-left').click(function(){
        $('.slick-carousel').slick("slickPrev");
    });
    
    // Hide first arrow
    var currentSlide = $('.slick-slider').slick('slickCurrentSlide');
    $('.arrow-left').toggle(currentSlide != 0);
    $('.arrow-right').toggle(currentSlide != 2);
    
    $('.slick-slider').on('afterChange', function() {
        $('.arrow-left,.arrow-right').show();
    });
    
    // Add Numbering to Directions
    $('#directions ol >').each(function() {
        $(this).prepend("<span>" + ($(this).index() +1) + "</span> ");
    });
