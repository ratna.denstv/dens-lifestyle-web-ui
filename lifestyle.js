$(document).ready(function() {
    
    // Main Banner
    $('#banner-lifestyle .owl-carousel').owlCarousel({
        loop: false,
        dots: false,
        items: 1, 
        lazyLoad: true,
    })
    
    // Video Player
    jwplayer("player").setup({ 
        autostart: false,
        file: "http://cdnapi.kaltura.com/p/1878761/sp/187876100/playManifest/entryId/1_usagz19w/flavorIds/1_5spqkazq,1_nslowvhp,1_boih5aji,1_qahc37ag/format/applehttp/protocol/http/a.m3u8", 
        width: "100%",
        aspectratio: '16:9'
    }); 
    
    // jsSocials
    $(".share").jsSocials({
        showLabel: false,
        showCount: false,
        shares: ["email", "twitter", "facebook", "whatsapp"]
    });
    
    // Schedule Carousel
    $('#lifestyle-player .owl-carousel').owlCarousel({
        stagePadding: 0,
        loop: false,
        margin: 7,
        dots: false,
        nav: true,
        navText: [`<span class="ion-ios-arrow-left"></span>`,`<span class="ion-ios-arrow-right "></span>`],          
        responsive : {
            0 :  {
                items: 3,
            },
            340 : { 
                items: 3,
            },
            600 : { 
                items: 5,
            },
            768 : {
                items: 7,
            },
            1024 : { 
                items: 3,
            },
            1280 : { 
                items: 3,
            },
            1366 : { 
                items: 4,
            },
            1600 : { 
                items: 5,
            },
            1920 : { 
                items: 5,
            },            
        },
        
    })  
    
    // Custom Scrollbar
    // $(".epg-list").mCustomScrollbar();
    
    // Searchbar Autocomplete
    var options = {
        data: ["blue", "green", "pink", "red", "yellow"],
        placeholder: "Search Dens Life&Style"
    };
    
    $("#searchbar").easyAutocomplete(options); 
    
    // Categories Carousel
    $('.lifestyle-carousel').owlCarousel({
        loop: false,
        margin: 10,
        nav: true,
        navText: [`<span class="ion-ios-arrow-left"></span>`,`<span class="ion-ios-arrow-right "></span>`],    
        dots: false,
        responsive:{
            0:{
                items:2
            },
            600:{
                items:2
            },
            800:{
                items:3
            },            
            1000:{
                items:4
            },
            1280: {
                items:5
            },
            1920: {
                items: 6
            }
        }
    })    
    
    // Hover Function
    $(".kotak").hover(function(){
        $('> .kotak_hover', this).show();
    },function(){
        $('> .kotak_hover', this).hide();
    });
    
    // down button clicked
    $('.bawah a').on("click", function () {
        
        event.preventDefault();
        
        // remove another active cascade
        $('.cascade').remove();
        
        var img = $(this).parents('.kotak').find('.img-carousel').attr('src');
        
        $(this).parents('.category-wrapper').find('.lifestyle-cascade').html(`
        <div class="cascade">
        <div class="inner-cascade">
        <img src="${img}" alt="" class="img-responsive">
        
        <div class="overlay">
        <img class="closeCascade" src="http://dens.tv/others/images/close-button.svg">
        <div class="content col-xs-12 col-sm-9 col-md-6">
        <h3>Tipe-tipe Pertempuran dan Ekonomi di Game World of Warships</h3>
        <div class="channel">
        <img class="channel_logo" src="http://whatson.dens.tv/media/img/wp/1573195277_whatson_v2.jpg" alt="News">
        <h5 class="channel_name"> News</h5>
        </div>
        <div class="clearfix"></div>
        <p>Video yang dipandu oleh Shinta Naomi ini menjelaskan mengenai tipe-tipe pertempuran yang ada di game World of Warships. Seperti kita ketahui, game ini merupakan game PC yang bertem ...</p>
        <br>
        <a href="#" class="viewmore">Read More</a>
        </div>
        </div>
        
        
        </div>
        </div>`).fadeIn();
        
        $(window).scrollTop($(this).offset().top);
        
        // close cascade on click
        $('.closeCascade').on("click", function () {
            
            $('.inner-cascade').fadeOut('slow');
            $('.cascade').fadeOut('slow');

        })
        
        
    });  
    
    
    
})

