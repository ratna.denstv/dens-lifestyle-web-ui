// kotak hover
$(".kotak").hover(function(){
    $('> .kotak_hover', this).show();
},function(){
    $('> .kotak_hover', this).hide();
});


// down button clicked
$('.bawah a').on("click", function () {
    
    event.preventDefault();
    
    // remove another active cascade
    $('.cascade').remove();
    
    var img = $(this).parents('.kotak').find('img').attr('src');

    $(this).parents('.kotak').after(`
    <div class="cascade">
    <div class="inner-cascade">
    <img src="${img}" alt="" class="img-responsive">
    
    <div class="overlay">
    <img class="closeCascade" src="http://dens.tv/others/images/close-button.svg">
    <div class="content col-xs-12 col-sm-9 col-md-6">
    <h3>Article Title Name</h3>
    <div class="channel">
    <img class="channel_logo" src="http://whatson.dens.tv/media/img/wp/1573195277_whatson_v2.jpg" alt="News">
    <h5 class="channel_name"> News</h5>
    </div>
    <div class="clearfix"></div>
    <p>Write article description here. Write article description here. Write article description here. Write article description here. Write article description here. Write article description here. Write article description here. ...</p>
    <br>
    <a href="article.html" class="viewmore">Read More</a>
    </div>
    </div>
    
    
    </div>
    </div>`).fadeIn();
    
    $(window).scrollTop($(this).offset().top);
    
    // close cascade on click
    $('.closeCascade').on("click", function () {
        
        $('.inner-cascade').fadeOut('slow');
        $('.cascade').fadeOut('slow');
        
    })
    
    
});  